desktopFirstTemplate = """.%s {
    @media screen and (max-width: $q-hd-screen) {
    }

    @media screen and (max-width: $high-hd-screen) {
    }

    @media screen and (max-width: $full-hd-screen) {
    }

    @media screen and (max-width: $dell-hd-screen) {
    }

    @media screen and (max-width: $mac-hd-screen) {
    }

    @media screen and (max-width: $ready-hd-screen) {
    }

    @media screen and (max-width: $big-tablet-screen) {
    }

    @media screen and (max-width: $middle-tablet-screen) {
    }

    @media screen and (max-width: $small-tablet-screen) {
    }

    @media screen and (max-width: $big-phone-screen) {
    }

    @media screen and (max-width: $middle-phone-screen) {
    }

    @media screen and (max-width: $small-phone-screen) {
    }
}\n\n""";

mobileFirstTemplate = """.%s {
    @media screen and (min-width: $xxs-screen) {
    }

    @media screen and (min-width: $xs-screen) {
    }

    @media screen and (min-width: $sm-screen) {
    }

    @media screen and (min-width: $md-screen) {
    }

    @media screen and (min-width: $lg-screen) {
    }

    @media screen and (min-width: $xl-screen) {
    }

    @media screen and (min-width: $xxl-screen) {
    }

    @media screen and (min-width: $xxl2-screen) {
    }
}\n\n""";

template=desktopFirstTemplate;
