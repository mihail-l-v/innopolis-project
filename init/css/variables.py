# iPhones
# 320
# 375
# 414
#
# Tablet
# 768
# 834
# 1024
#
# Desktop
# 1200
# 1350
# 1550
# 1750
#
# xK
# 2500
# 3400

variables = """/* screen sizes */
$q-hd-screen: 3451px;
$high-hd-screen: 2561px;
$full-hd-screen: 1930px;
$dell-hd-screen: 1660px;
$mac-hd-screen: 1450px;
$ready-hd-screen: 1290px;

$big-tablet-screen: 1030px;
$middle-tablet-screen: 850px;
$small-tablet-screen: 780px;

$small-phone-screen: 320px;
$middle-phone-screen: 375px;
$big-phone-screen: 414px;""";
